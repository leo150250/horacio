function adicionarNovoProfessor() {
    div_conteudo.innerHTML="";
    novoTitulo=document.createElement("h1");
    novoTitulo.innerHTML="Adicionando novo professor";
    div_conteudo.appendChild(novoTitulo);
    novoForm=document.createElement("form");
    novoForm.appendChild(adicionarItemForm("nome","Nome completo","text"));
    novoForm.appendChild(adicionarItemForm("cpf","CPF","text"));
    novoForm.appendChild(adicionarItemForm("foto","Foto","file"));
    novoForm.appendChild(adicionarItemForm("submit","Adicionar","submit"));
    div_conteudo.appendChild(novoForm);
}
function adicionarItemForm(argIdForm,argLabel,argInput) {
    divNovoItem=document.createElement("div");
    if (argInput=="submit") {
        divNovoItem.classList.add("formSubmit");
    } else {
        pNovoItem=document.createElement("p");
        pNovoItem.innerHTML=argLabel;
        divNovoItem.appendChild(pNovoItem);
    }
    inputNovoItem=document.createElement("input");
    inputNovoItem.id=argIdForm;
    inputNovoItem.name=argIdForm;
    inputNovoItem.type=argInput;
    divNovoItem.appendChild(inputNovoItem);
    return divNovoItem;
}

adicionarNovoProfessor();