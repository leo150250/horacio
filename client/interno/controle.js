const div_controle = document.createElement("div");
div_controle.id="controle";
div_controle.classList.add("barra");
div_controle.classList.add("topo");
div_wrapper.appendChild(div_controle);

function controle_criarBotao(argNome,argLabel,argAcao) {
    var novoBotao = document.createElement("button");
    novoBotao.title=argNome;
    novoBotao.innerHTML=argLabel;
    novoBotao.setAttribute("onclick",argAcao);
    div_controle.appendChild(novoBotao);
}

controle_criarBotao("Menu","M","location.href='index.html'");
controle_criarBotao("Cadastros","C","location.href='cadastros.html'");